import java.util.Scanner;

class Fahrkartenautomat2
{
    public static void main(String[] args)
    {
    	
    	//Aufgabe A2.5 Eingabe der Anzahl der Fahrkarten
    	
    	// Ein neuer Scanner wird f�r die Engabe der Ticketanzahl ben�tigt.
    	Scanner tickets = new Scanner(System.in);
    	Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       // Eine neue Variable f�r die Ticketanzahl wird definiert.
       int anzahlTickets;
       double ticketpreis = 3.50;

       // Eine neue Abfrage mit anschlie�ender Eingabe wird erstellt.
       System.out.print("Der Ticketpreis betr�gt ");
       System.out.printf("%.2f", ticketpreis);
       System.out.println(" Euro.");
       System.out.print("Wie viele Tickets ben�tigen Sie? ");
       anzahlTickets = tickets.nextInt();
       //F�r diese Aufgabe wird der zu zahlende Betrag vom System errechnet, daher ist eine Eingabe nicht notwendig
       //System.out.print("Zu zahlender Betrag (EURO): ");
       //zuZahlenderBetrag = tastatur.nextDouble();
       
       //Das System errechnet den Ticketpreis
       zuZahlenderBetrag = anzahlTickets * ticketpreis;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   // Anpassen des Betrags auf zwei Nachkommastellen.
    	   System.out.print("Noch zu zahlen: " );
    	   System.out.printf("%.2f%n", + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
    	   //Auch hier Anpassung des Betrags auf zwei Nachkommastellen.
    	   System.out.print("Der R�ckgabebetrag in H�he von ");
    	   System.out.printf("%.2f", r�ckgabebetrag);
    	   System.out.println(" EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
       tastatur.close();
       tickets.close();
    }
}