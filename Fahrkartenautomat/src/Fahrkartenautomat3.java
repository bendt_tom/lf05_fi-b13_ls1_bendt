
import java.util.Scanner;

public class Fahrkartenautomat3 {

	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double rueckgabebetrag;
		
		
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rueckgabebetrag);
		
		System.out.println("\nVergessen Sie nicht den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"+
	               "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	 public static double fahrkartenbestellungErfassen() {
		 Scanner tickets = new Scanner(System.in);
		 double ticketpreis = 3.5;
			System.out.print("Der Ticketpreis betr�gt ");
			System.out.printf("%.2f", ticketpreis);
			System.out.println(" Euro.");
			System.out.print("Wie viele Tickets ben�tigen Sie? ");
			int anzahlTickets = tickets.nextInt();
			
			return anzahlTickets * ticketpreis;
			}
	 public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		 Scanner tastatur = new Scanner(System.in);
		 double eingezahlterGesamtbetrag = 0.0;
		 double eingeworfeneM�nze;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.print("Noch zu zahlen: ");
	    	   System.out.printf("%.2f%n", + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
	       return zuZahlenderBetrag;
	 }
	 public static void fahrkartenAusgeben() {
	 System.out.println("\nFahrschein wird ausgegeben");
     for (int i = 0; i < 8; i++)
     {
        System.out.print("=");
        try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     }
     System.out.println("\n\n");
     }
	 public static double rueckgeldAusgeben(double rueckgabebetrag) {
			double r�ckgabebetrag;
			double eingezahlterGesamtbetrag= 0.0;
			double zahlenderBetrag=0.0;
			r�ckgabebetrag = eingezahlterGesamtbetrag - zahlenderBetrag;
		       if(r�ckgabebetrag > 0.0)
		       {
		    	   System.out.print("Der R�ckgabebetrag in H�he von ");
		    	   System.out.printf("%.2f", r�ckgabebetrag);
		    	   System.out.println(" EURO");
		    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

		           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
		           {
		        	  System.out.println("2 EURO");
			          r�ckgabebetrag -= 2.0;
		           }
		           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
		           {
		        	  System.out.println("1 EURO");
			          r�ckgabebetrag -= 1.0;
		           }
		           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
		           {
		        	  System.out.println("50 CENT");
			          r�ckgabebetrag -= 0.5;
		           }
		           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
		           {
		        	  System.out.println("20 CENT");
		 	          r�ckgabebetrag -= 0.2;
		           }
		           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
		           {
		        	  System.out.println("10 CENT");
			          r�ckgabebetrag -= 0.1;
		           }
		           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
		           {
		        	  System.out.println("5 CENT");
		 	          r�ckgabebetrag -= 0.05;
		           }
		       }
		      return rueckgabebetrag; 
		}
}