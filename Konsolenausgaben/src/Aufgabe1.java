
public class Aufgabe1 {

	public static void main(String[] args) {
		//Konsolenausgabe 1
		//Aufgabe 1
		String b = "Beispielsatz";
		System.out.println("\"Dies ist ein "+ b +".\"");
		System.out.println("\"Ist dies auch ein "+ b +"?\"");
		//Unterschied print println: bei println wird zusätzlich ein Zeilenumbruch nach der Ausgabe eingefügt.
	
	
	//Aufgabe 2
		String a = "*************";
		System.out.printf("%7.1s\n", a);
		System.out.printf("%8.3s\n", a);
		System.out.printf("%9.5s\n", a);
		System.out.printf("%10.7s\n", a);
		System.out.printf("%11.9s\n", a);
		System.out.printf("%12.11s\n", a);
		System.out.printf("%13.13s\n", a);
		System.out.printf("%8.3s\n", a);
		System.out.printf("%8.3s\n", a);
		
	//Aufgabe 3
		double c = 22.4234234;
		double d = 111.2222;
		double e = 4.0;
		double f = 1000000.551;
		double g = 97.34;
		
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
		System.out.printf("%.2f\n", f);
		System.out.printf("%.2f\n", g);
	
	}

}
