
public class Aufgabe2 {

	public static void main(String[] args) {
		//Konsolenausgabe 2
		//Aufgabe 1
		
		System.out.println("Aufgabe 1");
		System.out.println();
		String a = "**";
		System.out.printf("%5.2s\n", a);
		System.out.printf("%.1s", a);
		System.out.printf("%7.1s\n", a);
		System.out.printf("%.1s", a);
		System.out.printf("%7.1s\n", a);
		System.out.printf("%5.2s\n", a);
		
		System.out.println("Aufgabe 2");
		System.out.println();
		
		//Aufgabe 2
		System.out.printf("%-5s", "0!");
		System.out.print("=");
		System.out.printf("%19s", "");
		System.out.print(" =");
		System.out.printf("%4s\n","1");
		
		System.out.printf("%-5s", "1!");
		System.out.print("=");
		System.out.printf("%19s", "1");
		System.out.print(" =");
		System.out.printf("%4s\n","1");
		
		System.out.printf("%-5s", "2!");
		System.out.print("=");
		System.out.printf("%19s", "1 * 2");
		System.out.print(" =");
		System.out.printf("%4s\n","2");
		
		System.out.printf("%-5s", "3!");
		System.out.print("=");
		System.out.printf("%19s", "1 * 2 * 3");
		System.out.print(" =");
		System.out.printf("%4s\n","6");
		
		System.out.printf("%-5s", "4!");
		System.out.print("=");
		System.out.printf("%19s", "1 * 2 * 3 * 4");
		System.out.print(" =");
		System.out.printf("%4s\n","24");
		
		System.out.printf("%-5s", "1!");
		System.out.print("=");
		System.out.printf("%19s", "1 * 2 * 3 * 4 * 5");
		System.out.print(" =");
		System.out.printf("%4s\n","120");
		
	}

}
